import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';
import { Directive, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { getDistanceFromTop } from '../functions/get-distance-from-top';
import { EventService } from '../services/event.service';

@Directive({
  selector: '[rbScrollDraw]'
})
export class ScrollDrawDirective implements OnInit, OnDestroy{

  sub:Subscription;

  viewportHeight:number = window.innerHeight || document.documentElement.clientHeight;

  @Input() from:{[key:string]:string} = {'opacity': '0'};
  @Input() to:{[key:string]:string} = {'opacity': '1'};
  @Input() proportion:number = 4/5;
  @Input() speed = '500ms';

  player:AnimationPlayer;

  constructor(
    private _event:EventService,
    public elRef:ElementRef,
    private _ab:AnimationBuilder
  ) { }

  ngOnInit(){
    let factory:AnimationFactory = this._ab.build([
      style(this.from)
    ]);
    let player:AnimationPlayer = factory.create(this.elRef.nativeElement);
    let playCount:number = 0;
    player.play();
    factory = this._ab.build([
      style(this.from),
      animate(`${this.speed} ease-out`, style(this.to)),
      style(this.to)
    ]);
    player = factory.create(this.elRef.nativeElement);
    this.sub = this._event.scroll.subscribe((event) => {
      const scrollArea:HTMLDivElement = event.target as HTMLDivElement;
      const top = getDistanceFromTop(this.elRef);
      if(top - scrollArea.scrollTop - this.viewportHeight*this.proportion < 0){
        if(playCount==0){
          player.play();
          this.sub.unsubscribe();
        }
        playCount++;
      }
    });
  }

  ngOnDestroy(){
    if(this.sub){ this.sub.unsubscribe(); }
  }

}

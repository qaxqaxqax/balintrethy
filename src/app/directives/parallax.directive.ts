import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { getDistanceFromTop } from '../functions/get-distance-from-top';
import { EventService } from '../services/event.service';

@Directive({
  selector: '[rbParallax]'
})
export class ParallaxDirective {

  sub:Subscription;

  viewportHeight:number = window.innerHeight || document.documentElement.clientHeight;
  prevScrollTop:number = 0;
  translate:{x:number, y:number} = {x: 0, y: 0};

  @Input() maxX = 30;
  @Input() maxY = 30;

  @Input() proportionX = 0;
  @Input() proportionY = -50;


  constructor(
    private _event:EventService,
    public elRef:ElementRef,
    private _renderer: Renderer2
  ) { }

  ngOnInit(){
    this._renderer.setStyle(this.elRef.nativeElement, 'transform', `translate(${this.translate.x}%,${this.translate.y}%)`);
    this._renderer.setStyle(this.elRef.nativeElement, 'transition', 'transform 500ms ease');
    this.sub = this._event.scroll.subscribe((event) => {
      const scrollArea:HTMLDivElement = event.target as HTMLDivElement;

      const top = getDistanceFromTop(this.elRef);
      if(top - scrollArea.scrollTop - this.viewportHeight < 0 && top - scrollArea.scrollTop + this.elRef.nativeElement.clientHeight > 0){
        const delta:number = scrollArea.scrollTop - this.prevScrollTop;
        if(this.proportionX!=0){
          if(this.translate.x + (delta / this.proportionX) <= this.maxX){
            this.translate.x = this.translate.x + (delta / this.proportionX);
          }
        }
        if(this.proportionY!=0){
          if(this.translate.y + (delta / this.proportionY) <= this.maxY){
            this.translate.y = this.translate.y + (delta / this.proportionY);
          }
        }
        this._renderer.setStyle(this.elRef.nativeElement, 'transform', `translate(${this.translate.x}%,${this.translate.y}%)`);
      }else if(top - scrollArea.scrollTop - this.viewportHeight < 0 ){
        this.translate.x = 0;
        this.translate.y = 0;
        this._renderer.setStyle(this.elRef.nativeElement, 'transform', `translate(${this.translate.x}%,${this.translate.y}%)`);
      }else if(top - scrollArea.scrollTop + this.elRef.nativeElement.clientHeight > 0){
        if(this.proportionX){ this.translate.x = this.maxX; }
        if(this.proportionY){ this.translate.y = this.maxY; }
        this._renderer.setStyle(this.elRef.nativeElement, 'transform', `translate(${this.translate.x}%,${this.translate.y}%)`);
        
      }

      this.prevScrollTop = scrollArea.scrollTop;
    });
  }

  ngOnDestroy(){
    if(this.sub){ this.sub.unsubscribe(); }
  }
  
  

}

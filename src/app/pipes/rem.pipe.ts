import { Pipe, PipeTransform } from '@angular/core';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { rem } from '../types/unit';

@Pipe({
  name: 'rem'
})
export class RemPipe implements PipeTransform {

  constructor(
    private _sanitizer: DomSanitizer
  ){}

  transform(value:number, sanitize:boolean=false):SafeStyle {
    return sanitize ? this._sanitizer.bypassSecurityTrustStyle(rem(value)) : rem(value);
	}

}

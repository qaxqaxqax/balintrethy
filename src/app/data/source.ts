import { Applicant } from "../types/applicant";

export const balintrethy:Applicant = {
  role: "FULL STACK WEB DEVELOPER",
  firstName: "BÁLINT",
  lastName: "RÉTHY",
  profilePicture: "/assets/images/landing/profile.png",
  brief: `I’m a Full Stack Web Developer with a good sense of design. I love unique & creative websites. I like games, music, photography, films & anime, art and esports. My hobbies are home renovation and long distance running.`,
  socials: [
    {
      name: "Gmail",
      icon: "/assets/images/social/email.svg",
      link: "mailto:rbalint25@gmail.com?subject=You%20have%20received%20a%20job%20offer!%20%3A)&body=Hello%20Mr.%20B%C3%A1lint%20R%C3%A9thy!%0D%0A%0D%0A..."
    },
    {
      name: "Twitter",
      icon: "/assets/images/social/twitter.svg",
      link: "https://twitter.com/rbalint2"
    },
    {
      name: "Gitlab",
      icon: "/assets/images/social/gitlab.svg",
      link: "https://gitlab.com/qaxqaxqax"
    },
    {
      name: "Github",
      icon: "/assets/images/social/github.svg",
      link: "https://github.com/qaxqaxqax"
    },
    {
      name: "Stackoverflow",
      icon: "/assets/images/social/stack.svg",
      link: "https://stackoverflow.com/users/9014621/b%c3%a1lint-r%c3%a9thy"
    }
  ],
  from: "Hungary",
  birth: "31 August 1995",
  experienceInYears: "8+",
  aboutMe: {
    brief: `Hi! My name is Bálint Réthy. I've started creating websites when I was 13 years old. I have always been attracted to modern and creative web solutions.`,
    career: `I was born in Hungary, in Veszprém, where I finished my schooling. I was interested in many different things and was always open to new technologies, so besides the schools I was engaged in my own web-oriented training. During this time I've made about 200 educational videos about mathematics, programming and design. My YouTube channel is called TheHUNTutorials. After school I've started working and took on a lot of side projects. I have tried out many programming languages and technologies, but what I really liked was always complex web applications, so I've specialized in that direction. I work well in my area of expertise both alone and in teams. I consider it at least as important that the end result looks and works well as that the underlying code is clean, well structured and simple as possible.`,
    me: `I consider myself a creative, open minded, proactive and precise person, who enjoys teamwork and who is always willing to face new challanges. My hobbies are long distance running and home renovation. For the latter point I am somewhat skilled in electrical installation, plasterboard installation, plastering, painting, plumbing, hot and cold cladding. I also made my own smart lighting system at home using Arduino.`,
    aim: `Last but not least, for the future, I would like to strengthen my portfolio with leadership experience.`,
    signo: "/assets/images/signo.svg"
  },
  resume: {
    download: '/assets/resume/balint_rethy_cv.pdf',
    experience: {
      limit: 3,
      jobs: [
        {
          period: "2023 - NOW",
          position: "Senior Application Developer",
          company: "ORACLE",
          description: `I am responsible to maintain, ship and enhance Oracle Guded Learning as part of a multinational team. I am proud about the product we develop makes it easier for our customers to provide a real life solution on complex workflows.`,
          milestones: [
            "Working in a top tier company",
            "Docker, Kubernetes and Oracle cloud experience",
            "Multinational team",
            "Working from home"
          ]
        },
        {
          period: "2020 - NOW",
          position: "Senior Software Engineer",
          company: "GRAPHISOFT SE",
          description: `I was maintaining a monolithic application called Portal, which consists of several Angular projects that I wrote and implemented. The main parts are the Test Framework Portal, the Result Processor and the Quality Control projects. `,
          milestones: [
            "Working method transition to Kanban",
            "Member of the UX/UI Guild",
            "Managing new members",
            "Full time home office"
          ]
        },
        {
          period: "2018 - 2020",
          position: "Test Automation Framework Engineer 2",
          company: "GRAPHISOFT SE",
          description: `The development of a new evaluation tool was essential to speed up the testing infrastructure. The Result Processor was developed as an application based on Angular, Koa, MySQL NGINX. Its main feature list supported checkpoint-based evaluation in distributed test suites, meaningful visualization of large text and bitmap differences, masking, video playback of test logs and much more.`,
          milestones: [
            "Agile transformation from waterfall modell",
            "Member of the Web Guild",
            "Educational presentations for ‘large’ audience",
            "Full stack development"
          ]
        },
        {
          period: "2017 - 2018",
          position: "Test Automation Framework Engineer",
          company: "GRAPHISOFT SE",
          description: `I started to maintain and develop the local autotest framework in Perl. My main development was a resource based dictionary, which allowed to test different interfaces in different languages with the same code. I also refactored several web infrastructure and pages related to the test framework. I gave the Portal name for the new web project group.`,
          milestones: [
            "Started at Graphisoft",
            "Complex Angular application development",
            "Clean Code",
            "Understanding essentials of testing"
          ]
        },
        {
          period: "2016 summer",
          position: "Fullstack Developer",
          company: "LB-KNAUF",
          description: `As part of my university internship, I independently created a website for internal use. The site can be used to manage various leave policies, security requirements, file sharing and exchange of various data and messages. Technologies used: apache, PHP, jQuery.`,
          milestones: [
            "One of my first complex web project",
            "Independent, creative and precise work"
          ]
        },
        {
          period: "2015 - 2016",
          position: "Junior Frontend Developer",
          company: "CREATIC",
          description: `I've created websites and landing pages for local restaurants, business, marketing and graphic design companies.`,
          milestones: [
            "Starting to use Angular and TypeScript",
            "A lot of small websites"
          ]
        }
      ]
    },
    expertise: [
      "Fullstack Web Development",
      "Software Engineering",
      "UI/UX Design"
    ],
    education: [
      {
        period: "2014 - 2017",
        degree: "Bachelor’s degree in Software Engineering",
        school: "University of Pannonia"
      },
      {
        period: "2010 - 2014",
        degree: "Information Technology",
        school: "Industrial Vocational High School and Gymnasium"
      }
    ],
    courses: [
      {
        year: "2021",
        titles: [
          "Kanban Course"
        ],
        placeof: "GRAPHISOFT SE"
      },
      {
        year: "2021",
        titles: [
          "Getting Started in User Experience",
          "Universal Principles of Design",
          "UX for Non-designers",
          "UX Design for Developers"
        ],
        placeof: "LinkedIn"
      },
      {
        year: "2019",
        titles: [
          "Agile Course"
        ],
        placeof: "GRAPHISOFT SE"
      },
      {
        year: "2018",
        titles: [
          "Clean Code"
        ],
        placeof: `Robert "Uncle Bob" Martin`
      },
      {
        year: "2016",
        titles: [
          `Cisco Certified Network Associate Security (CCNAS)`,
          "Coach Program"
        ],
        placeof: "University of Pannonia"
      },
      {
        year: "2015",
        titles: [
          `Cisco Certified Network Associate 1 (CCNA1)`,
          "Self Marketing English Course"
        ],
        placeof: "University of Pannonia"
      },
      {
        year: "2014",
        titles: [
          `Cisco Certified Network Associate 2 (CCNA2)`
        ],
        placeof: "Industrial Vocational High School and Gymnasium"
      },
      {
        year: "2013",
        titles: [
          `Cisco Certified Network Associate 1 (CCNA1)`,
        ],
        placeof: "Industrial Vocational High School and Gymnasium"
      }
    ],
    awards: [
      {
        year: "2019",
        title: "Graphisoft Innovation Day 5th place",
        description: `It was a 1-8 team competition about innovating ideas that can make change. Me and my colleague won the 5th place prize with our ‘Video Log’ project.`
      }
    ],
    languages: [
      {
        name: "Hungarian",
        level: "Mother tongue"
      },
      {
        name: "English",
        level: "B2 degree",
        placeof: "University of Pannonia"
      }
    ],
    personality: {
      code: "ENTJ-T",
      placeof: "16personalities.com"
    },
    licences: [
      "B category driving license"
    ],
    hobbies: [
      "Games",
      "Home renovation",
      "Long distance running",
      "Cooking",
      "Music",
      "Art"
    ],
    applications: [
      {
        name: "Microsoft Office",
        details: [
          { 
            name: "Word",
            level: "advanced"
          },
          {
            name: "PowerPoint",
            level: "advanced"
          },
          {
            name: "Excel",
            level: "advanced"
          },
          {
            name: "Access",
            level: "advanced"
          },
          {
            name: "Outlook",
            level: "advanced"
          },
          {
            name: "SharePoint",
            level: "advanced"
          }
        ]
      },
      { 
        name: "Google Workspace",
        level: "advanced"
      },
      {
        name: "Adobe",
        details: [
          { 
            name: "Photoshop",
            level: "expert"
          },
          {
            name: "Lightroom",
            level: "basic"
          },
          {
            name: "Illustrator",
            level: "expert"
          },
          {
            name: "After Effects",
            level: "expert"
          },
          {
            name: "Indesign",
            level: "basic"
          },
          {
            name: "Premiere Pro",
            level: "advanced"
          },
          {
            name: "XD",
            level: "advanced"
          }
        ]
      },
      { 
        name: "Figma",
        level: "advanced"
      },
      { 
        name: "Sony Vegas Pro",
        level: "expert"
      },
      { 
        name: "Blender",
        level: "advanced"
      },
      { 
        name: "Cinema 4D",
        level: "advanced"
      },
      { 
        name: "GIMP",
        level: "expert"
      },
      { 
        name: "Paint.NET",
        level: "expert"
      },
      { 
        name: "Flame Painter",
        level: "expert"
      },
      { 
        name: "Magic Bullet Looks",
        level: "expert"
      },
      { 
        name: "Inkscape",
        level: "basic"
      },
      { 
        name: "Oracle VM",
        level: "advanced"
      },
      { 
        name: "VMware",
        level: "basic"
      },
      { 
        name: "MYSQLWorkbench",
        level: "advanced"
      },
      { 
        name: "MongoDB Compass",
        level: "advanced"
      },
      { 
        name: "Postman",
        level: "advanced"
      },
      {
        name: "Editors",
        details: [
          { 
            name: "Notepad++",
            level: "expert"
          },
          {
            name: "Sublime Text",
            level: "expert"
          },
          {
            name: "Code::Blocks",
            level: "advanced"
          },
          {
            name: "Qt Creator",
            level: "advanced"
          },
          {
            name: "NetBeans",
            level: "basic"
          },
          {
            name: "Ms Visual Studio",
            level: "basic"
          },
          {
            name: "Ms Visual Studio Code",
            level: "expert"
          }
        ]
      },
      { name: "..." }
    ],
    languagesAndTechnologies: [
      { 
        name: "C++ & Qt",
        level: "expert"
      },
      { 
        name: "C",
        level: "expert"
      },
      { 
        name: "Arduino (UNO & ESP8266)",
        level: "advanced"
      },
      { 
        name: "C#",
        level: "basic"
      },
      { 
        name: "Kinect V2",
        level: "basic"
      },
      { 
        name: "Java",
        level: "basic"
      },
      { 
        name: "Python",
        level: "advanced"
      },
      { 
        name: "Perl",
        level: "expert"
      },
      { 
        name: "Bash",
        level: "advanced"
      },
      { 
        name: "CLISP",
        level: "advanced"
      },
      { 
        name: "HTML",
        level: "expert"
      },
      { 
        name: "SVG",
        level: "expert"
      },
      { 
        name: "CSS (SASS/SCSS)",
        level: "expert"
      },
      { 
        name: "PHP",
        level: "expert"
      },
      { 
        name: "JavaScript",
        level: "expert"
      },
      { 
        name: "TypeScript",
        level: "expert"
      },
      { 
        name: "NodeJS (Express/Koa)",
        level: "advanced"
      },
      { 
        name: "NestJS",
        level: "advanced"
      },
      { 
        name: "AJAX",
        level: "expert"
      },
      { 
        name: "Angular",
        level: "expert"
      },
      { 
        name: "React",
        level: "basic"
      },
      { 
        name: "Web Workers",
        level: "advanced"
      },
      { 
        name: "Service Workers",
        level: "advanced"
      },
      { 
        name: "Canvas",
        level: "advanced"
      },
      { 
        name: "NX",
        level: "advanced"
      },
      { 
        name: "JWT",
        level: "advanced"
      },
      { 
        name: "ThreeJS",
        level: "advanced"
      },
      { 
        name: "Socket.io",
        level: "advanced"
      },
      { 
        name: "REST API",
        level: "expert"
      },
      { 
        name: "GraphQL",
        level: "advanced"
      },
      { 
        name: "Chai & Mocha",
        level: "advanced"
      },
      { 
        name: "Jasmine & Karma",
        level: "advanced"
      },
      { 
        name: "Jest & Cypress",
        level: "advanced"
      },
      { 
        name: "MongoDB",
        level: "advanced"
      },
      { 
        name: "MySQL",
        level: "advanced"
      },
      { 
        name: "MSSQL",
        level: "advanced"
      },
      { 
        name: "Electron",
        level: "advanced"
      },
      { 
        name: "JQUERY (& UI)",
        level: "advanced"
      },
      { 
        name: "UML",
        level: "advanced"
      },
      { 
        name: "Git",
        level: "advanced"
      },
      { 
        name: "Perforce",
        level: "advanced"
      },
      { 
        name: "Docker & Kubernetes",
        level: "advanced"
      },
      { 
        name: "Clean Code",
        level: "advanced"
      },
      { name: "..." }
    ]
  }
};
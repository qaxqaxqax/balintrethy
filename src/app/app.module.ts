import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { IntroductionComponent } from './components/introduction/introduction.component';
import { AboutmeComponent } from './components/aboutme/aboutme.component';
import { ButtonComponent } from './components/button/button.component';
import { ResumeComponent } from './components/resume/resume.component';
import { JobComponent } from './components/job/job.component';
import { DetailBlockComponent } from './components/detail-block/detail-block.component';
import { MarkableComponent } from './components/markable/markable.component';
import { ScrollDrawDirective } from './directives/scroll-draw.directive';
import { EventService } from './services/event.service';
import { RemPipe } from './pipes/rem.pipe';
import { ParallaxDirective } from './directives/parallax.directive';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    IntroductionComponent,
    AboutmeComponent,
    ButtonComponent,
    ResumeComponent,
    JobComponent,
    DetailBlockComponent,
    MarkableComponent,
    ScrollDrawDirective,
    RemPipe,
    ParallaxDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    EventService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export interface SocialEndpoint{
  name:string,
  icon:string,
  link:string
}
export interface Education{
  period:string,
  degree:string,
  school:string
}
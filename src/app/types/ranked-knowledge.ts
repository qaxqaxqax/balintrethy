export interface RankedKnowledge{
  name:string,
  level?:string,
  details?:RankedKnowledge[]
}
export interface Award{
  year:string,
  title:string,
  description:string
}
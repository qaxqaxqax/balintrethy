export const BASE_FONT_SIZE:number = 10;

export function pxToRem(pixel:number){
	return BASE_FONT_SIZE*pixel/Number(window.getComputedStyle(document.querySelector("html")).getPropertyValue('font-size').split('px')[0]);
}

export function remToPx(rem:number){
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize)/BASE_FONT_SIZE;
}

export function rem(value:number){
  return (value/BASE_FONT_SIZE)+'rem';
}
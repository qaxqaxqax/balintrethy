export interface Job{
  period:string,
  position:string,
  company:string,
  description:string,
  milestones:string[]
}
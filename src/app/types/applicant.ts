import { AboutMe } from "./about-me";
import { Resume } from "./resume";
import { SocialEndpoint } from "./social-endpoint";

export interface Applicant{
  role:string,
  firstName:string,
  lastName:string,
  profilePicture:string,
  brief:string,
  socials:SocialEndpoint[],
  from:string,
  birth:string,
  experienceInYears:string,
  aboutMe:AboutMe,
  resume:Resume
}
import { Award } from "./award";
import { Course } from "./course";
import { Education } from "./education";
import { Experience } from "./experience";
import { Language } from "./language";
import { Personality } from "./personality";
import { RankedKnowledge } from "./ranked-knowledge";

export interface Resume{
  experience:Experience,
  expertise:string[],
  education:Education[],
  courses:Course[],
  awards:Award[],
  languages:Language[],
  personality:Personality,
  licences:string[],
  hobbies:string[],
  applications:RankedKnowledge[],
  languagesAndTechnologies:RankedKnowledge[],
  download:string
}
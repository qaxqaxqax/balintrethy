export interface AboutMe{
  brief:string,
  career:string,
  me:string,
  aim:string,
  signo:string
}
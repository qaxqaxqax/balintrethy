import { Job } from "./job";

export interface Experience{
  limit:number,
  jobs:Job[]
}
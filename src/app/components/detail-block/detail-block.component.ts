import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'rb-detail-block',
  templateUrl: './detail-block.component.html',
  styleUrls: ['./detail-block.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetailBlockComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Job } from 'src/app/types/job';

@Component({
  selector: 'rb-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobComponent implements OnInit {

  @Input('data') job:Job;

  constructor() { }

  ngOnInit(): void {
    
  }

}

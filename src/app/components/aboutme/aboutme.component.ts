import { EventEmitter, Input } from '@angular/core';
import { Component, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { AboutMe } from 'src/app/types/about-me';

@Component({
  selector: 'section[rbAboutme]',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutmeComponent implements OnInit {

  @Input('data') aboutMe:AboutMe;

  @Output() viewResume:EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  constructor() { }

  ngOnInit(): void {
  }

  onViewResume(event:MouseEvent){
    this.viewResume.emit(event);
  }

}

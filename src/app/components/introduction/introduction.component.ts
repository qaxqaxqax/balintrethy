import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Applicant } from 'src/app/types/applicant';

@Component({
  selector: 'section[rbIntroduction]',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IntroductionComponent implements OnInit {

  @Input('data') applicant:Applicant;

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, ElementRef, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Resume } from 'src/app/types/resume';

@Component({
  selector: 'section[rbResume]',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeComponent implements OnInit {

  @Input('data') resume:Resume;
  showAll:boolean = false;

  constructor(
    public elRef:ElementRef<HTMLElement>
  ) { }

  ngOnInit(): void {
  }

  onShowAllExperience(event:MouseEvent){
    this.showAll = true;
  }

}

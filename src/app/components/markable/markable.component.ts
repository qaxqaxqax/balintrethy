import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'rb-markable',
  templateUrl: './markable.component.html',
  styleUrls: ['./markable.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host:{
    '[class.marked]': "marked"
  }
})
export class MarkableComponent implements OnInit {

  @Input() marked:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}

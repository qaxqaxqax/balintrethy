import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkableComponent } from './markable.component';

describe('MarkableComponent', () => {
  let component: MarkableComponent;
  let fixture: ComponentFixture<MarkableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

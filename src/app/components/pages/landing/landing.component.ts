import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { balintrethy } from 'src/app/data/source';
import { Applicant } from 'src/app/types/applicant';
import { ResumeComponent } from '../../resume/resume.component';

@Component({
  selector: 'rb-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LandingComponent implements OnInit {

  @ViewChild(ResumeComponent) resumeRef:ResumeComponent;

  applicant:Applicant = balintrethy;

  constructor() { }

  ngOnInit(): void {

  }

  onViewResume(event:MouseEvent){
    this.resumeRef.elRef.nativeElement.scrollIntoView({behavior: 'smooth'});
  }

}

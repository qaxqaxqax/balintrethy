import { ElementRef } from "@angular/core";

export function getDistanceFromTop(elRef:ElementRef){
  let elem = elRef.nativeElement;
  let distance = 0;
  do {
    distance += elem.offsetTop;
    elem = elem.offsetParent;
  } while (elem);
  distance = distance < 0 ? 0 : distance;
  return distance;
}